<?php

namespace PortalAirsoft\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    private $ormType = 'Eloquent';

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $logicasPath = app_path().DIRECTORY_SEPARATOR."Domain";
        $logicasPathContent = scandir($logicasPath);
        foreach ($logicasPathContent as $item) {
            if ($item != "." && $item != "..") {
                $this->app->bind(
                    'App\Domain\\'.$item.'\\Model\\'.$item.'Repository',
                    'App\Domain\\'.$item.'\\Model\\'.$item.'Repository'.$this->ormType
                );
            }
        }
    }
}
