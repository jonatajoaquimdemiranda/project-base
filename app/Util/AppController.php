<?php
namespace PortalAirsoft\Util;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PortalAirsoft\Http\Controllers\Controller;

class AppController extends Controller
{
    protected $repository;
    protected $service;
    protected $appResponse;
    protected $noPaginate = false;
    protected $with;

    public function __construct(AppResponse $appResponse)
    {
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if ($this->with) {
            $with = $this->with;
        } else {
            $with = $this->repository->makeModel()->parents();
        }
        if ($this->noPaginate) {
            $return = $this->nxResponse
                ->Response($this->repository
                ->with($with)
                ->all());
        } else {
            $paginate = isset($params['limit']) && $params['limit'] != '' ? (int)$params['limit'] : 30;
            $return = $this->repository
                ->with($with)
                ->paginate($paginate);
        }
        if(isset($params['export'])){
            $return = $return->toArray()['data'];
            $export = new NxExport($this->nxResponse);
            $return = $export->export($return, $params['export']);
        }
        return $return;
    }

    public function store(Request $request)
    {
        return $this->nxResponse
            ->Response($this->service->create($request->all()));
    }

    public function show(Request $request, $id)
    {
        $params = $request->all();

        $return = $this->nxResponse
            ->Response($this->service->show($id));

        if(isset($params['export'])){
            $arrayCollection = $this->service->show($id)->toArray();
            $export = new NxExport($this->nxResponse);
            $return = $export->export($arrayCollection, $params['export']);
        }
        return $return;
    }

    public function update(Request $request, $id)
    {
        return $this->nxResponse
            ->Response($this->service->update($request->all(), $id));
    }

    public function destroy($id)
    {
        if ($this->repository->delete($id)) {
            return $this->nxResponse
                ->Response("O registro foi excluído!");
        }
        throw new BusinessException("Erro ao excluir o registro!");
    }
}
