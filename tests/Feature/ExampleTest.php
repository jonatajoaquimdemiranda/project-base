<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $logicasPath = app_path().DIRECTORY_SEPARATOR."Domain";
        $logicasPathContent = scandir($logicasPath);
        foreach ($logicasPathContent as $item) {
            if ($item != "." && $item != "..") {
                echo $item."\n";
                $testDomain = $this->get('/api/'.$item);
                echo "\tqtd registros: ".count($testDomain->json()['data'])."\n";
                $testDomain->assertStatus(200);
            }
        }
        foreach ($logicasPathContent as $item) {
            if ($item != "." && $item != "..") {
                echo $item."\n";

                $classNameService = "\\App\\Domain\\".$item."\\".$item."Service";
                $classNameModel = "\\App\\Domain\\".$item."\\Model\\".$item;
                $service = $this->app->make($classNameService);
                $arrayValuesToPost = $service->getFakerFillable($classNameModel);

                $this->post('/api/'.$item, $arrayValuesToPost)->assertStatus(200);
            }
        }
        foreach ($logicasPathContent as $item) {
            if ($item != "." && $item != "..") {
                echo $item."\n";
                $testDomain = $this->get('/api/'.$item);
                echo "\tqtd registros: ".count($testDomain->json()['data'])."\n";
                $testDomain->assertStatus(200);
            }
        }
    }
}
